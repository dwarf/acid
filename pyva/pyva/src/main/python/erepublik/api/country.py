import feed
from feed import XmlFeed, get_json

country_feed = None

def get_cached_countries():
	global country_feed
	
	if country_feed == None:
		country_feed = XmlFeed('http://api.erepublik.com/v2/feeds/countries/')
	
	return country_feed
	
def from_id(id):
	return get_json('http://api.1way.it/erep/country/%d' % int(id))

def from_name(name):
	country_feed = get_cached_countries()
	
	countries = country_feed.elements('/countries/country')
	name = name.lower()
	
	for c in countries:
		if name == c.text('name').lower() or name == c.text('code').lower():
			return from_id(c.text('id/text()'))
	
	return None

def from_dict(dict):
	return from_id(dict['id'])

def get_taxes(country_id):
	return get_json('http://api.1way.it/erep/tax/%d' % country_id)

def get_regions(country_id):
	return get_json('http://api.1way.it/erep/regions/%d' % country_id)
