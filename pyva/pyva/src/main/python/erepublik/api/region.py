import map
import utils
from decimal import Decimal
from feed import XmlFeed

def from_id(id):
	return Region(XmlFeed('http://api.erepublik.com/v2/feeds/regions/%d' % int(id)))

def from_name(name):
	id = map.get_region_id(name)

	if id == None:
		return None

	return from_id(id)

def from_dict(dict):
	return from_id(dict['id'])

class Region:
	"""A region of Erepublik"""

	def __init__(self, f):
		self.id = f.int('/region/id')
		self.name = f.text('/region/name')
		self.population = f.int('/region/citizen-count')
		self.country = {'name': map.get_country_name(f.int('/region/country/id')), 'id': f.int('/region/country/id')}

		hospitals = f.elements('/region/buildings/hospitals/hospital')

		if hospitals == None:
			self.hospitals = None
		else:
			self.hospitals = [{
				'area': h.int('area-of-effect'), 'budget': h.int('wellness-budget'), 'quality': h.int('customization-level'),
				'max_heal': h.int('max-heal-per-citizen')} for h in hospitals]

		defense = f.elements('/region/buildings/defense-systems/defense-system')

		if defense == None:
			self.defense = None
		else:
			self.defense = [{
				'area': ds.int('area-of-effect'), 'bonus': ds.int('defense-bonus'), 'quality': ds.int('customization-level'),
				'durability': ds.int('durability')} for ds in defense]

		self.raw_materials = f.text('/region/raw-materials/name')

		self.neighbours = [{
			'country': {'name': map.get_country_name(neighbour.int('country/id')), 'id': neighbour.int('country/id')},
			'region': {'name': map.get_region_name(neighbour.int('region/id')), 'id': neighbour.int('region/id')}}
			for neighbour in f.elements('/region/neighbours/neighbour')]

		self.attack_cost = 25 + Decimal(self.population) / 10

	def __str__(self):
		return '[%d] %s' % (self.id, self.name)
