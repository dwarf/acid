import feed
from feed import XmlFeed

def from_id(id):
	return War(XmlFeed('http://api.erepublik.com/v1/feeds/war/%d' % int(id)))

class War:
	"""Erepublik war"""
	
	def __init__(self, f):
		self.battles = [{battle.int('battle-id'): {
			'loser_id': battle.int('looser-id'), 'was_secured': battle.bool('was-secured'),
			'is_active': battle.int('is-active'), 'region_id': battle.int('region-id'),
			'region': battle.text('region'), 'battle_link': battle.text('battle-link'),
			'winner_id': battle.int('winner-id')}} for battle in f.elements('/war/battles/battle')]
			
		self.fetched_at = f.text('/war/info/fetched-at')
		self.is_resistance = f.text('/war/info/is-for-independence')
		self.expires_in = f.int('/war/info/expires-in')
		self.war_link = f.text('/war/info/war-link')
		self.created_at = f.text('/war/info/created-at') #TODO: date

