import time
import logging
from pseudoclient.sys_base import *
from api import feed, news, region

class NewsManager(Subsystem):
	def __init__(self, module):
		Subsystem.__init__(self, module, module.options, 'news')
		self.set_option('last_timestamp', int(time.time()))
		self.log = logging.getLogger(__name__)

	def parse(self, message):
		return message

	def commit(self):
		try:
			last_update = self.get_option('last_timestamp', int, int(time.time()))
			events = news.get()
			cur_update = int(time.mktime(events.updated))

			if cur_update <= last_update:
				self.set_option('last_timestamp', cur_update)
				return

			messages = []

			for entry in events.entries:
				if int(time.mktime(entry['updated'])) > last_update:
					messages.append(self.parse(entry['title']))

			self.set_option('last_timestamp', cur_update)

			self.log.warning("About to spam eRepublik News")

			for channel in self.module.channels.list_valid():
				if channel.news:
					self.module.multimsg(channel.name, 4, '@nsep @bNEWS@b @nsep ', ' @nsep ', messages, ' @nsep')
		except Exception, err:
			import traceback
			traceback.print_exc()
			self.module.elog.error('News broadcast failed: @b%s@b' % err)
