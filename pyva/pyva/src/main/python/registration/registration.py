#!/usr/bin/python pseudoserver.py
# psm_registration.py
# module for pypseudoserver
# written by B (ben@rizon.net)
# helpful registration bot!

from pyva import *
import logging
from core import *
from plugin import *

import pyva_net_rizon_acid_core_Acidictive as Acidictive
import pyva_net_rizon_acid_core_User as User
import pyva_net_rizon_acid_core_Channel as Channel

class registration(AcidPlugin):
	def __init__(self):
		AcidPlugin.__init__(self)

		self.name = "registration"
		self.log = logging.getLogger(__name__)
		
	def start(self):
		self.client = self.config.get('registration', 'nick')
		return True
		
	def onChanModes(self, prefix, chan, modes):
		if modes != "+z":
			return #ignore all other mode changes, we only want +z for starts

		me = User.findUser(self.client)
		if not me:
			return

		user = User.findUser(prefix)
		if not user or user.getNick() != 'ChanServ':
			return

		self.log.debug("Caught a channel registration")
		
		me.joinChan(chan)
		
		for line in self.config.get('registration', 'text').split('\n'):
			Acidictive.privmsg(self.client, chan, line) #line.strip()

		#logging fun
		Acidictive.privmsg(self.client, self.logchan, "registration info sent to %s" % chan)
		
		c = Channel.findChannel(chan)
		if c:
			me.partChan(c)
