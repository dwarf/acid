import time
from sys_base import *
from core import anope_major

class Request(object):
	def __init__(self, user, channel, action):
		self.user = user
		self.channel = channel
		self.action = action
		self.time = int(time.time())

class AuthManager(Subsystem):
	def __init__(self, module):
		Subsystem.__init__(self, module, module.options, 'auth')
		self.requests = {}

	def request(self, user, channel, action):
		if user in self.requests:
			self.module.notice(user, 'Sorry, you already have a pending request. Please retry later.')
			return
		
		self.requests[user] = Request(user, channel, action)
		
		if self.onRequest(user, action, channel):
			pass
		elif anope_major == 1:
			self.module.msg('ChanServ', 'WHY %s %s' % (channel, user))
		elif anope_major == 2:
			self.module.msg('ChanServ', 'STATUS %s %s' % (channel, user))

	def accept(self, user):
		if not user in self.requests:
			return

		request = self.requests[user]
		action = request.action
		channel = request.channel
		
		if self.onAccept(user, request, action, channel):
			pass
		elif action == 'request':
			if not channel in self.module.channels:
				self.module.channels.add(channel)
				self.module.notice(user, 'Joined @b%s@b.' % channel)
		elif action == 'remove':
			if self.module.channels.is_valid(channel):
				self.module.channels.remove(channel)
				self.module.notice(user, 'Parted @b%s@b.' % channel)
		else:
			self.module.elog.request('Invalid request. Action: @b%s@b. Channel: @b%s@b. User: @b%s@b.' % (action, channel, user))
			self.module.notice(user, 'Invalid request: @b%s@b.' % action)
			del self.requests[user]
			return

		self.module.elog.request('Request accepted. Action: @b%s@b. Channel: @b%s@b. User: @b%s@b.' % (action, channel, user))
		del self.requests[user]

	def onRequest(self, user, action, channel):
		return False

	def onAccept(self, user, request, action, channel):
		return False

	def reject_not_founder(self, user, channel):
		self.reject(user, 'You are not the channel founder of @b%s@b.' % channel)
		
	def reject_not_registered(self, channel):
		for user in self.requests:
			if self.requests[user].channel == channel:
				self.reject(user, 'Channel @b%s@b is unregistered.' % channel)
				break

	def reject(self, user, reason = ''):
		if not user in self.requests:
			return

		request = self.requests[user]
		action = request.action
		channel = request.channel
		self.module.elog.request('Request rejected. Action: @b%s@b. Channel: @b%s@b. User: @b%s@b. Reason: @b%s@b.' % (action, channel, user, reason))
		self.module.notice(user, reason)
		del self.requests[user]

	def commit(self):
		timeout_time = int(time.time()) - 20
		expired_requests = []
		
		for user in self.requests:
			request = self.requests[user]
			
			if request.time < timeout_time:
				expired_requests.append(user)
				action = request.action
				channel = request.channel
				
				try:
					self.module.elog.request('Request expired. Action: @b%s@b. Channel: @b%s@b. User: @b%s@b.' % (action, channel, user))
					self.module.notice(user, 'Request @b%s@b expired. Please retry later.' % action)
				except Exception, err:
					pass
		
		for user in expired_requests:
			del self.requests[user]
