import feed
from decimal import Decimal, ROUND_DOWN, ROUND_UP 

__ranks = [
	{'name': 'Rookie',                     'id': 1,  'modifier': 1,    'points': 0},
	{'name': 'Private',                    'id': 2,  'modifier': 1.1,  'points': 250},
	{'name': 'Private First Class',        'id': 3,  'modifier': 1.2,  'points': 1000},
	{'name': 'Corporal',                   'id': 4,  'modifier': 1.3,  'points': 3000},
	{'name': 'Sergeant',                   'id': 5,  'modifier': 1.4,  'points': 5000},
	{'name': 'Staff Sergeant',             'id': 6,  'modifier': 1.5,  'points': 10000},
	{'name': 'Sergeant First Class',       'id': 7,  'modifier': 1.6,  'points': 30000},
	{'name': 'Master Sergeant',            'id': 8,  'modifier': 1.65, 'points': 60000},
	{'name': 'First Sergeant',             'id': 9,  'modifier': 1.7,  'points': 100000},
	{'name': 'Sergeant Major',             'id': 10, 'modifier': 1.75, 'points': 250000},
	{'name': 'Command Sergeant Major',     'id': 11, 'modifier': 1.8,  'points': 500000},
	{'name': 'Sergeant Major of the Army', 'id': 12, 'modifier': 1.85, 'points': 750000},
	{'name': 'Second Lieutenant',          'id': 13, 'modifier': 1.9,  'points': 1000000},
	{'name': 'First Lieutenant',           'id': 14, 'modifier': 1.93, 'points': 1500000},
	{'name': 'Captain',                    'id': 15, 'modifier': 1.96, 'points': 2500000},
	{'name': 'Major',                      'id': 16, 'modifier': 2,    'points': 5000000},
	{'name': 'Lieutenant Colonel',         'id': 17, 'modifier': 2.03, 'points': 10000000},
	{'name': 'Colonel',                    'id': 18, 'modifier': 2.06, 'points': 17000000},
	{'name': 'Brigadier General',          'id': 19, 'modifier': 2.1,  'points': 25000000},
	{'name': 'Major General',              'id': 20, 'modifier': 2.12, 'points': 35000000},
	{'name': 'Lieutenant General',         'id': 21, 'modifier': 2.14, 'points': 45000000},
	{'name': 'General',                    'id': 22, 'modifier': 2.16, 'points': 60000000},
	{'name': 'General of the Army',        'id': 23, 'modifier': 2.18, 'points': 80000000},
	{'name': 'Marshall',                   'id': 24, 'modifier': 2.2,  'points': 100000000},
	{'name': 'Field Marshall',             'id': 25, 'modifier': 2.22, 'points': 125000000},
	{'name': 'Supreme Marshall',           'id': 26, 'modifier': 2.24, 'points': 175000000},
	{'name': 'Generalissimus',             'id': 27, 'modifier': 2.26, 'points': 250000000},
	{'name': 'Supreme Generalissimuss',    'id': 28, 'modifier': 2.28, 'points': 400000000},
	{'name': 'Imperial Generalissimus',    'id': 29, 'modifier': 2.4,  'points': 600000000},
	{'name': 'Legendary Generalissimuss',  'id': 30, 'modifier': 2.42, 'points': 800000000},
	{'name': 'Imperator',                  'id': 31, 'modifier': 2.44, 'points': 1000000000},
	{'name': 'Imperator Caesar',           'id': 32, 'modifier': 2.46, 'points': 1500000000},
	{'name': 'Deus Dimidiam',              'id': 33, 'modifier': 2.48, 'points': 2500000000},
	{'name': 'Deus',                       'id': 34, 'modifier': 2.5,  'points': 5000000000},
	{'name': 'Summi Deus',                 'id': 35, 'modifier': 2.52, 'points': 7500000000},
	{'name': 'Deus Imperialis',            'id': 36, 'modifier': 2.54, 'points': 10000000000},
	{'name': 'Deus Fabuloso',              'id': 37, 'modifier': 2.56, 'points': 15000000000},
	{'name': 'Deus Ultimum',               'id': 38, 'modifier': 2.58, 'points': 20000000000},
]

__regions = [
	{'id': 1,   'name': 'Mazovia'},
	{'id': 2,   'name': 'Silesia'},
	{'id': 3,   'name': 'Great Poland'},
	{'id': 4,   'name': 'Mazuria'},
	{'id': 5,   'name': 'Little Poland'},
	{'id': 6,   'name': 'Pomerania'},
	{'id': 7,   'name': 'Moscow'},
	{'id': 8,   'name': 'Western Russia'},
	{'id': 9,   'name': 'Siberia'},
	{'id': 10,  'name': 'North Caucasus'},
	{'id': 11,  'name': 'Kamchatka'},
	{'id': 12,  'name': 'Northwestern Russia'},
	{'id': 13,  'name': 'Brandenburgia'},
	{'id': 14,  'name': 'Mecklenburg and Western Pomerania'},
	{'id': 15,  'name': 'Saxony'},
	{'id': 16,  'name': 'Bavaria'},
	{'id': 17,  'name': 'Rhineland'},
	{'id': 18,  'name': 'Baden-Wurttemberg'},
	{'id': 19,  'name': 'Central France'},
	{'id': 20,  'name': 'Northern France'},
	{'id': 21,  'name': 'Jerusalem'},
	{'id': 22,  'name': 'Rhone Alpes'},
	{'id': 23,  'name': 'Great West'},
	{'id': 24,  'name': 'Southwestern France'},
	{'id': 25,  'name': 'Alsace Lorraine'},
	{'id': 26,  'name': 'Andalusia'},
	{'id': 27,  'name': 'Aragon Catalonia'},
	{'id': 28,  'name': 'Madrid'},
	{'id': 29,  'name': 'Galicia'},
	{'id': 30,  'name': 'Castile Leon'},
	{'id': 31,  'name': 'Castile-La Mancha'},
	{'id': 32,  'name': 'London'},
	{'id': 33,  'name': 'Northeastern England'},
	{'id': 34,  'name': 'Wales'},
	{'id': 35,  'name': 'Southwestern England'},
	{'id': 36,  'name': 'Scotland'},
	{'id': 37,  'name': 'Northern Ireland'},
	{'id': 38,  'name': 'Sicily'},
	{'id': 39,  'name': 'Naples'},
	{'id': 40,  'name': 'Central Italy'},
	{'id': 41,  'name': 'Tuscany'},
	{'id': 42,  'name': 'Veneto'},
	{'id': 43,  'name': 'Lombardia Piemonte'},
	{'id': 44,  'name': 'Western Transdanubia'},
	{'id': 45,  'name': 'Eastern Transdanubia'},
	{'id': 46,  'name': 'Central Hungary'},
	{'id': 47,  'name': 'Southern Great Plain'},
	{'id': 48,  'name': 'Northern Great Plain'},
	{'id': 49,  'name': 'Northern Great Hungary'},
	{'id': 50,  'name': 'Crisana Banat'},
	{'id': 51,  'name': 'Wallachia'},
	{'id': 52,  'name': 'Dobrogea'},
	{'id': 53,  'name': 'Transylvania'},
	{'id': 54,  'name': 'Moldova'},
	{'id': 55,  'name': 'Maramur Bucovina'},
	{'id': 56,  'name': 'Sofia'},
	{'id': 57,  'name': 'Plovdiv'},
	{'id': 58,  'name': 'Burgas'},
	{'id': 59,  'name': 'Varna'},
	{'id': 60,  'name': 'Ruse'},
	{'id': 61,  'name': 'Vidin'},
	{'id': 62,  'name': 'Western Serbia'},
	{'id': 63,  'name': 'Southern Serbia'},
	{'id': 64,  'name': 'Sumadija'},
	{'id': 65,  'name': 'Eastern Serbia'},
	{'id': 66,  'name': 'Vojvodina'},
	{'id': 67,  'name': 'Belgrade'},
	{'id': 68,  'name': 'Slavonia'},
	{'id': 69,  'name': 'Central Croatia'},
	{'id': 70,  'name': 'Dalmatia'},
	{'id': 71,  'name': 'Kvarner'},
	{'id': 72,  'name': 'Istria'},
	{'id': 73,  'name': 'Zagreb'},
	{'id': 74,  'name': 'Herzegovina'},
	{'id': 75,  'name': 'Sarajevo'},
	{'id': 76,  'name': 'Northern Bosnia'},
	{'id': 77,  'name': 'Posavina'},
	{'id': 78,  'name': 'Central Bosnia'},
	{'id': 79,  'name': 'Bosanska Krajina'},
	{'id': 80,  'name': 'Crete'},
	{'id': 81,  'name': 'Peloponnese'},
	{'id': 82,  'name': 'Central Greece'},
	{'id': 83,  'name': 'Epirus Thessaly'},
	{'id': 84,  'name': 'Makedonia'},
	{'id': 85,  'name': 'Thrace'},
	{'id': 86,  'name': 'Western Republic of Macedonia'},
	{'id': 87,  'name': 'Pelagonia'},
	{'id': 88,  'name': 'Vardar'},
	{'id': 89,  'name': 'Skopje'},
	{'id': 90,  'name': 'Southeastern Republic of Macedonia'},
	{'id': 91,  'name': 'Northeastern Republic of Macedonia'},
	{'id': 92,  'name': 'Kiev'},
	{'id': 93,  'name': 'Western Ukraine'},
	{'id': 94,  'name': 'Central Ukraine'},
	{'id': 95,  'name': 'Eastern Ukraine'},
	{'id': 96,  'name': 'Black Sea Coast'},
	{'id': 97,  'name': 'Crimea'},
	{'id': 98,  'name': 'Smaland Scania'},
	{'id': 99,  'name': 'Gotaland'},
	{'id': 100, 'name': 'Svealand'},
	{'id': 101, 'name': 'Bohus'},
	{'id': 102, 'name': 'Jamtland'},
	{'id': 103, 'name': 'Norrland Sameland'},
	{'id': 104, 'name': 'Northern Portugal'},
	{'id': 105, 'name': 'Beira'},
	{'id': 106, 'name': 'Estremaduria Ribatejo'},
	{'id': 107, 'name': 'Alentejo'},
	{'id': 108, 'name': 'Algarve'},
	{'id': 109, 'name': 'Azores'},
	{'id': 110, 'name': 'Lithuania Eastern highland'},
	{'id': 111, 'name': 'Sudovia'},
	{'id': 112, 'name': 'Lithuania Western highland'},
	{'id': 113, 'name': 'Samogitia'},
	{'id': 114, 'name': 'Lithuania minor'},
	{'id': 115, 'name': 'Dainava'},
	{'id': 116, 'name': 'Eastern Vidzeme'},
	{'id': 117, 'name': 'Latgalia'},
	{'id': 118, 'name': 'Selija'},
	{'id': 119, 'name': 'Western Vidzeme'},
	{'id': 120, 'name': 'Zemgale'},
	{'id': 121, 'name': 'Kurzeme'},
	{'id': 122, 'name': 'Prekmurje'},
	{'id': 123, 'name': 'Styria Carinthia'},
	{'id': 124, 'name': 'Upper Carniola'},
	{'id': 125, 'name': 'Slovenian Littoral'},
	{'id': 126, 'name': 'Lower Carniola'},
	{'id': 127, 'name': 'Inner Carniola'},
	{'id': 128, 'name': 'Marmara'},
	{'id': 129, 'name': 'Turkey Aegean Coast'},
	{'id': 130, 'name': 'Turkey Mediterranean Coast'},
	{'id': 131, 'name': 'Turkey Black Sea Coast'},
	{'id': 132, 'name': 'Eastern Anatolia'},
	{'id': 133, 'name': 'Central Anatolia'},
	{'id': 134, 'name': 'North of Brazil'},
	{'id': 135, 'name': 'Northeast of Brazil'},
	{'id': 136, 'name': 'Southeast of Brazil'},
	{'id': 137, 'name': 'Central Brazil'},
	{'id': 138, 'name': 'Parana and Santa Catarina'},
	{'id': 139, 'name': 'Rio Grande do Sul'},
	{'id': 140, 'name': 'Patagonia'},
	{'id': 141, 'name': 'Pampas'},
	{'id': 142, 'name': 'Cuyo'},
	{'id': 143, 'name': 'Tucuman'},
	{'id': 144, 'name': 'Chaco'},
	{'id': 145, 'name': 'Mesopotamia'},
	{'id': 146, 'name': 'Northern Mexico'},
	{'id': 147, 'name': 'Mexico Pacific Coast'},
	{'id': 148, 'name': 'Yucatan Peninsula'},
	{'id': 149, 'name': 'Central Mexico'},
	{'id': 150, 'name': 'The Bajio'},
	{'id': 151, 'name': 'Baja California'},
	{'id': 152, 'name': 'Alaska'},
	{'id': 153, 'name': 'USA Pacific Coast'},
	{'id': 154, 'name': 'Western USA'},
	{'id': 155, 'name': 'Central USA'},
	{'id': 156, 'name': 'USA East Coast'},
	{'id': 157, 'name': 'USA Gulf of Mexico'},
	{'id': 158, 'name': 'Northern Canada'},
	{'id': 159, 'name': 'Canada East Coast'},
	{'id': 160, 'name': 'Quebec'},
	{'id': 161, 'name': 'Manitoba'},
	{'id': 162, 'name': 'Alberta'},
	{'id': 163, 'name': 'Canada Pacific Coast'},
	{'id': 164, 'name': 'Manchuria'},
	{'id': 165, 'name': 'Northwest China'},
	{'id': 166, 'name': 'Tibet'},
	{'id': 167, 'name': 'North China'},
	{'id': 168, 'name': 'South China'},
	{'id': 169, 'name': 'East China'},
	{'id': 170, 'name': 'Sumatra'},
	{'id': 171, 'name': 'Java'},
	{'id': 172, 'name': 'Kalimantan'},
	{'id': 173, 'name': 'Sulawesi'},
	{'id': 174, 'name': 'Papua'},
	{'id': 175, 'name': 'Maluku Islands'},
	{'id': 176, 'name': 'Western Iran'},
	{'id': 177, 'name': 'Northern Iran'},
	{'id': 178, 'name': 'Khorasan'},
	{'id': 179, 'name': 'Southeastern Iran'},
	{'id': 180, 'name': 'Fars'},
	{'id': 181, 'name': 'Central Iran'},
	{'id': 182, 'name': 'Jeolla'},
	{'id': 183, 'name': 'Gyeongsang'},
	{'id': 184, 'name': 'Chuncheon'},
	{'id': 185, 'name': 'Gyeonggi'},
	{'id': 186, 'name': 'North Chungcheong'},
	{'id': 187, 'name': 'South Chungcheong'},
	{'id': 188, 'name': 'Penghu'},
	{'id': 189, 'name': 'Taipei'},
	{'id': 190, 'name': 'Northeastern Taiwan'},
	{'id': 191, 'name': 'Southeastern Taiwan'},
	{'id': 192, 'name': 'Kaohsiung'},
	{'id': 193, 'name': 'Western Taiwan'},
	{'id': 194, 'name': 'Nazareth'},
	{'id': 195, 'name': 'Judea and Samaria'},
	{'id': 196, 'name': 'Beersheba'},
	{'id': 197, 'name': 'Haifa'},
	{'id': 198, 'name': 'Coastal Plain'},
	{'id': 199, 'name': 'Southern India'},
	{'id': 200, 'name': 'Western India'},
	{'id': 201, 'name': 'Bengal Coast'},
	{'id': 202, 'name': 'Central India'},
	{'id': 203, 'name': 'Northern India'},
	{'id': 204, 'name': 'Eastern India'},
	{'id': 205, 'name': 'North Holland-Utrecht'},
	{'id': 206, 'name': 'South Holland-Zeeland'},
	{'id': 207, 'name': 'Western Australia'},
	{'id': 208, 'name': 'Northern Territory'},
	{'id': 209, 'name': 'South Australia'},
	{'id': 210, 'name': 'Queensland'},
	{'id': 211, 'name': 'New South Wales'},
	{'id': 212, 'name': 'Victoria'},
	{'id': 213, 'name': 'Groningen-Drenthe'},
	{'id': 214, 'name': 'Friesland-Flevoland'},
	{'id': 215, 'name': 'Gelderland-Overijssel'},
	{'id': 216, 'name': 'Brabant-Limburg'},
	{'id': 217, 'name': 'Western Finland'},
	{'id': 218, 'name': 'Aland'},
	{'id': 219, 'name': 'Eastern Finland'},
	{'id': 220, 'name': 'Lapland'},
	{'id': 221, 'name': 'Southern Finland'},
	{'id': 222, 'name': 'Oulu'},
	{'id': 223, 'name': 'Cork/Kerry'},
	{'id': 224, 'name': 'Shannon'},
	{'id': 225, 'name': 'South-east Ireland'},
	{'id': 226, 'name': 'North-west Ireland'},
	{'id': 227, 'name': 'Western Ireland'},
	{'id': 228, 'name': 'Midlands-Ireland'},
	{'id': 229, 'name': 'Zurich'},
	{'id': 230, 'name': 'Ticino-Grisons'},
	{'id': 231, 'name': 'Northeastern Switzerland'},
	{'id': 232, 'name': 'Central Switzerland'},
	{'id': 233, 'name': 'Bern-Valais'},
	{'id': 234, 'name': 'Western Switzerland'},
	{'id': 235, 'name': 'Antwerpen - Limburg'},
	{'id': 236, 'name': 'Brussels'},
	{'id': 237, 'name': 'Vlaanderen'},
	{'id': 238, 'name': 'Hainaut - Namur'},
	{'id': 239, 'name': 'Liege'},
	{'id': 240, 'name': 'Luxembourg'},
	{'id': 241, 'name': 'Sindh'},
	{'id': 242, 'name': 'Balochistan'},
	{'id': 243, 'name': 'Punjab'},
	{'id': 244, 'name': 'Northern areas'},
	{'id': 245, 'name': 'North West Frontier Province'},
	{'id': 246, 'name': 'Tribal areas'},
	{'id': 247, 'name': 'Sarawak'},
	{'id': 248, 'name': 'Sabah'},
	{'id': 249, 'name': 'Kuala Lumpur'},
	{'id': 250, 'name': 'Pahang'},
	{'id': 251, 'name': 'Northern Peninsular-Malaysia'},
	{'id': 252, 'name': 'Southern Peninsular-Malaysia'},
	{'id': 253, 'name': 'Svalbard'},
	{'id': 254, 'name': 'Nord-Norge'},
	{'id': 255, 'name': 'Trondelag'},
	{'id': 256, 'name': 'Sorlandet'},
	{'id': 257, 'name': 'Vestlandet'},
	{'id': 258, 'name': 'Ostlandet'},
	{'id': 259, 'name': 'Loreto'},
	{'id': 260, 'name': 'Lima'},
	{'id': 261, 'name': 'Southern coast of Peru'},
	{'id': 262, 'name': 'Northwestern Peru'},
	{'id': 263, 'name': 'Peru Andes'},
	{'id': 264, 'name': 'Central Peru'},
	{'id': 265, 'name': 'Punta Arenas'},
	{'id': 266, 'name': 'Puerto Montt'},
	{'id': 267, 'name': 'Santiago'},
	{'id': 268, 'name': 'Atacama'},
	{'id': 269, 'name': 'Antofagasta'},
	{'id': 270, 'name': 'Tarapaca'},
	{'id': 271, 'name': 'Amazonica'},
	{'id': 272, 'name': 'Orinoquia'},
	{'id': 273, 'name': 'Pacifica'},
	{'id': 274, 'name': 'Caribe'},
	{'id': 275, 'name': 'Bogota'},
	{'id': 276, 'name': 'Andino'},
]

class InvalidValueError(Exception):
	def __init__(self, message):
		self.msg = message

	def __str__(self):
		return str(self.msg)

def fight_calc(firepower, rank, strength, region_bonus, ds_bonus,
			   mu_bonus, surrounded_debuff):
	firepowers = {
				  0: 0.5, # No weapon
				  1: 1.2, # Weapon quality 1
				  2: 1.4, # Weapon quality 2
				  3: 1.6, # Weapon quality 3
				  4: 1.8, # Weapon quality 4
				  5: 2.0, # Weapon quality 5
				 }
	q_mult  = Decimal(firepowers[firepower])
	r_mult  = Decimal(rank['modifier'])
	s_mult  = Decimal(strength)
	ds_mult = Decimal(1.0 + (float(ds_bonus) * 0.05))
	mu_mult = Decimal(1.0 + (float(mu_bonus) / 100.0))
	
	dmg = q_mult * r_mult * s_mult * ds_mult * mu_mult
	if region_bonus:
		dmg *= Decimal('1.2')
	if surrounded_debuff:
		dmg *= Decimal('0.8')
	return dmg.to_integral_value(ROUND_DOWN)

def get_rank(rank_name):
	rank_name = rank_name.lower()
	
	for rank in __ranks:
		if rank_name == rank['name'].lower() or rank_name == str(rank['id']):
			return rank

	raise InvalidValueError('%s is not a valid military rank' % rank_name)

def get_region_by_name(name):
	name = name.lower()
	for region in __regions:
		if name == region['name'].lower():
			return region
	raise feed.FeedError('Region @b%s@b does not exist.' % name)

def get_region_by_id(id):
	for region in __regions:
		if id == region['id']:
			return region
	raise feed.FeedError('Region ID @b%s@b does not exist.' % str(id))


if __name__ == '__main__':
	import json
	import urllib2
	api = urllib2.urlopen('http://primera.e-sim.org/apiRanks.html')
	data = json.load(api)
	api.close()
	
	print '__ranks = ['
	s = "	{'name': %%-%ds 'id': %%-3s 'modifier': %%-5s 'points': %%s},"
	longest = 0
	for i in data:
			if len(i['name']) > longest:
					longest = len(i['name'])
	
	longest += 3
	for i in range(len(data)):
			print s % longest % (
								 "'%s'," % data[i]['name'],
								 "%s," % (i+1),
								 "%s," % data[i]['damageModifier'],
								 data[i]['damageRequired'],
								)
	print ']'
