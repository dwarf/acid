from plugin import AcidPlugin
from plugin import modules, getPlugins
from core import *
import time
from istring import istring
import os

import pyva_net_rizon_acid_core_Acidictive as Acidictive
import pyva_net_rizon_acid_core_User as User

class control(AcidPlugin):
	def __init__(self):
		AcidPlugin.__init__(self)
		self.name = "control"
		self.control = config.get('control', 'nick')
		self.controlchannel = config.get('control', 'channel')

	def cmd_status(self, source, target, pieces):

		try:
			las = [str(x)[:4] for x in os.getloadavg()]
		except:
			las = ["unk", "unk", "unk"]

		Acidictive.privmsg(self.control, target, "Load: [%s %s %s]" % (las[0], las[1], las[2]))
		Acidictive.privmsg(self.control, target, "Modules loaded: %s" % (", ".join(getPlugins())))
		return True

	def cmd_help(self, source, target, pieces):
		"""
		Prints a list of commands and syntax a user is allowed to use
		TODO: change output to be more segregated:
			`help` should list modules
			`help <module>` should list help for that module
			`help all` should list all commands
		"""
		
		if len(pieces) == 0 or len(pieces) > 1:
			Acidictive.notice(self.control, source, "Help is available for the following modules:")
			Acidictive.notice(self.control, source, "help all")
			for k,v in modules.iteritems():
				Acidictive.notice(self.control, source, "help %s" % v.name)
		elif len(pieces) == 1:
			if pieces[0] == "all":
				filter = None
			else:
				filter = pieces[0]

			cmds = []
			for modname in modules:
				mod = modules[modname]
				if mod.getCommands():
					for name,info in mod.getCommands():
						cmds.append([mod.name + '.' + name, info])

			user = User.findUser(source)
			for cmdname,cmdinfo in cmds:
				perm = cmdinfo['permission']
				if not perm or user.hasFlags(perm):
					if not perm:
						perm = " "
					if not filter or filter == cmdname.split('.')[0]:
						Acidictive.notice(self.control, source, "[%s] %s%s %s" % (perm, self.prefix, cmdname, cmdinfo['usage']))

		Acidictive.notice(self.control, source, "End of Help")
		return True

	def cmd_reload(self, source, target, pieces):

		try:
			config.read("config.ini")
			Acidictive.notice(self.control, source, "Reloaded configuration")
			return True
		except Exception, err:
			self.log.error("Error reloading config.ini (%s)" % err)
			return False

	def getCommands(self):
		return (
			('status', {
				'permission':'',
				'callback':self.cmd_status,
				'usage':"- display technical information"}),

			('help', {
				'permission':'',
				'callback':self.cmd_help,
				'usage':"- shows help for all commands available *to you*"}),

			('reload', {
				'permission':'r',
				'callback':self.cmd_reload,
				'usage':"- reloads certain configuration parameters"}),
		       )

	def onPrivmsg(self, creator, recipient, message):
		params = message.split(' ')
		user = User.findUser(creator)
		
		if not user or len(params) == 0 or len(params[0]) == 0 or params[0][0] != self.prefix or recipient != self.controlchannel:
			return
		
		cmd = params[0][1:]
		
		for modname in modules:
			mod = modules[modname]
			if not mod.getCommands():
				continue
			for cmdname, info in mod.getCommands():
				if mod.name + '.' + cmdname == cmd and (not info['permission'] or user.hasFlags(info['permission'])):
					try:
						try:
							msg = params[1:]
						except:
							msg = None
						
						if not info['callback'](creator, recipient, msg):
							Acidictive.notice(self.control, creator, "Usage: %s%s %s" % (self.prefix, cmd, info['usage']))
					except Exception, err:
						self.log.error('COMMAND: Error executing %s (%s)' % (cmd, err))
						import traceback
						traceback.print_exc()
						raise
