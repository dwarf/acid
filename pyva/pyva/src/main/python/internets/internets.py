#!/usr/bin/python pseudoserver.py
# psm_internets.py
# module for pypseudoserver
# written by ElChE <elche@rizon.net>, martin <martin@rizon.net>

import sys
import traceback
import types
import logging

from istring import istring
from datetime import datetime
from decimal import Decimal, InvalidOperation

from utils import *
from pyva import *
from core import *
from plugin import *
import mythreading as threading
from pseudoclient import sys_antiflood, sys_auth, sys_channels, sys_log, sys_options, cmd_manager, inviteable

import cmd_admin, cmd_user, erepparser, internets_users
from api import bing, calc, google, imdb, ipinfo, lastfm, quotes, urbandictionary, urls, weather, wolfram, words, steam
from internets_utils import *
from api.steam import SteamUser

import pyva_net_rizon_acid_core_Acidictive as Acidictive
import pyva_net_rizon_acid_core_AcidCore as AcidCore
import pyva_net_rizon_acid_core_User as User

class internets(
	AcidPlugin,
	inviteable.InviteablePseudoclient
):
	initialized = False

	def bind_function(self, function):
		func = types.MethodType(function, self, internets)
		setattr(internets, function.__name__, func)
		return func

	def bind_admin_commands(self):
		list = cmd_admin.get_commands()
		self.commands_admin = []

		for command in list:
			self.commands_admin.append(
				(command,
					{
						'permission': 'e',
						'callback': self.bind_function(list[command][0]),
						'usage': list[command][1]
					}
				)
			)

	def __init__(self):
		AcidPlugin.__init__(self)

		self.name = "internets"
		self.log = logging.getLogger(__name__)

		try:
			self.nick = istring(self.config.get('internets', 'nick'))
		except Exception, err:
			self.log.exception("Error reading 'internets:nick' configuration option: %s" % err)
			raise

		try:
			self.chan = istring(self.config.get('internets', 'channel'))
		except Exception, err:
			self.log.exception("Error reading 'internets:channel' configuration option: %s" % err)
			raise

		self.bind_admin_commands()

	def start_threads(self):
		self.options.start()
		self.channels.start()
		self.users.start()
		self.auth.start()
		self.antiflood.start()

	def start(self):
		try:
			AcidPlugin.start(self)
			inviteable.InviteablePseudoclient.start(self)

			self.options = sys_options.OptionManager(self)
			self.elog = sys_log.LogManager(self)
			self.commands_user = cmd_user.UserCommandManager()
		except Exception, err:
			self.log.exception('Error initializing core subsystems for internets module (%s)' % err)
			raise

		self.elog.debug('Started core subsystems.')

		try:
			self.channels = sys_channels.ChannelManager(self)
			self.users = internets_users.InternetsUserManager(self)
			self.auth = sys_auth.AuthManager(self)
			self.antiflood = sys_antiflood.AntiFloodManager(self)
		except Exception, err:
			self.log.exception('Error initializing subsystems for internets module (%s)' % err)
			raise

		self.elog.debug('Started subsystems.')

		try:
			try:
				self.bing = bing.Bing(self.config.get('internets', 'bing_appid'))
			except Exception, err:
				self.log.exception('Error initializing internets bing API (%s)' % err)
			self.nsp = calc.NumericStringParser()
			self.google = google.Google(self.config.get('internets', 'key_google'))
			self.imdb = imdb.Imdb()
			self.ipinfo = ipinfo.IpInfo(self.config.get('internets', 'key_ipinfodb'))
			self.lastfm = lastfm.LastFm(self.config.get('internets', 'key_lastfm'))
			self.quotes = quotes.Quotes(self.config.get('internets', 'key_fml'))
			self.urbandictionary = urbandictionary.UrbanDictionary()
			self.urls = urls.Urls(self.config.get('internets', 'user_bitly'), self.config.get('internets', 'key_bitly'))
			self.weather = weather.Weather(self.config.get('internets', 'key_openweathermap'))
			self.wolfram = wolfram.Wolfram(self.config.get('internets', 'key_wolframalpha'))
			self.wordnik = words.Words(self.config.get('internets', 'key_wordnik'))
			self.steam = steam.Steam(self.config.get('internets', 'key_steam'))
		except Exception, err:
			self.log.exception('Error initializing internets module (%s)' % err)
			raise

		for channel in self.channels.list_valid():
			self.join(channel.name)

		self.log.debug('Joined channels.')

		try:
			self.start_threads()
		except Exception, err:
			self.log.exception('Error starting threads for internets module (%s)' % err)
			raise

		self.initialized = True
		self.online = True
		self.elog.debug('Started threads.')
		return True

	def stop(self):
		if hasattr(self, 'antiflood'):
			self.antiflood.stop()

		if hasattr(self, 'auth'):
			self.auth.stop()

		if hasattr(self, 'users'):
			if self.initialized:
				self.users.force()

			self.users.stop()
			self.users.db_close()

		if hasattr(self, 'channels'):
			if self.initialized:
				self.channels.force()

			self.channels.stop()
			self.channels.db_close()

		if hasattr(self, 'options'):
			if self.initialized:
				self.options.force()

			self.options.stop()
			self.options.db_close()

	def errormsg(self, target, message):
		self.msg(target, '@b@c4Error:@o %s' % message)

	def usagemsg(self, target, description, examples):
		message = '@errsep @bUsage@b %s @errsep' % description

		if examples != None:
			message += ' @bExamples@b %s @errsep' % ', '.join(examples)

		self.msg(target, message)

	def msg(self, target, message):
		if message != '':
			Acidictive.privmsg(self.nick, target, format_ascii_irc(message))

	def multimsg(self, target, count, intro, separator, pieces, outro = ''):
		cur = 0

		while cur < len(pieces):
			self.msg(target, intro + separator.join(pieces[cur:cur + count]) + outro)
			cur += count

	def notice(self, target, message):
		if message != '':
			Acidictive.notice(self.nick, target, format_ascii_irc(message))

	def execute(self, manager, command, argument, channel, sender, userinfo):
		full_command = '%s%s' % (command, ' %s' % argument if len(argument) else '')
		cmd = manager.get_command(command)

		if cmd == None:
			self.msg(channel, manager.invalid)
			self.elog.debug('Parsed command @b%s@b: invalid command.' % full_command)
			return

		if self.users.is_banned(sender) or self.antiflood.check_user(sender, command, argument):
			user = self.users[sender]
			message = 'You were banned by @b%s@b.' % user.ban_source

			if user.ban_reason != None:
				message += ' Reason: @b%s@b.' % user.ban_reason

			if user.ban_expiry != None:
				message += ' Expires: @b%s@b.' % datetime.fromtimestamp(user.ban_expiry)

			self.notice(sender, message)
			self.elog.debug('Parsed command @b%s@b: user is banned.' % full_command)
			return

		self.elog.command('%s%s > %s' % (sender, ':%s' % channel if channel != sender else '', full_command))

		parser = erepparser.ErepublikParser(add_help_option = False, option_class = erepparser.ErepublikParserOption)
		cmd_type = cmd[1]
		cmd_args = cmd[3]

		parser.add_option('-?', '--help', action = 'store_true')

		for cmd_arg in cmd_args:
			parser.add_option(cmd_arg[1], '--' + cmd_arg[0], **cmd_arg[3])

		try:
			(popts, pargs) = parser.parse_args(args = argument.split(' '))
		except erepparser.ErepublikParserError, err:
			self.msg(channel, str(err)) #TODO: Avoid str, use unicode.
			parser.destroy()
			self.elog.debug('Parsed command @b%s@b: invalid options.' % full_command)
			return

		if popts.help == True:
			manager.commands['help'][0](self, manager, {}, command, channel, sender)
			parser.destroy()
			self.elog.debug('Parsed command @b%s@b: help intercepted.' % full_command)
			return

		opt_dict = {}
		larg = ' '.join(pargs).strip()
		is_offline = True

		for cmd_arg in cmd_args:
			parg = getattr(popts, cmd_arg[0])

			if parg != None:
				if len(cmd_arg) <= 4 or not (cmd_arg[4] & cmd_manager.ARG_OFFLINE):
					is_offline = False

				if len(cmd_arg) > 4 and (cmd_arg[4] & cmd_manager.ARG_YES) and larg == '':
					self.msg(channel, 'Error: %s option requires an argument.' % cmd_arg[1])
					parser.destroy()
					self.elog.debug('Parsed command @b%s@b: option constraint was broken.' % full_command)
					return

				opt_dict[cmd_arg[0]] = parg
			elif len(cmd_arg) > 4 and (cmd_arg[4] & cmd_manager.ARG_OFFLINE and cmd_arg[4] & cmd_manager.ARG_OFFLINE_REQ):
				is_offline = False

		if not self.online and ((len(pargs) > 0 and not (cmd_type & cmd_manager.ARG_OFFLINE)) or not is_offline):
			self.notice(sender, 'The eRepublik API is offline. Please retry later.')
			parser.destroy()
			self.elog.debug('Parsed command @b%s@b: offline.' % full_command)
			return

		if (cmd_type & cmd_manager.ARG_YES) and (larg == None or larg == ''):
			self.notice(sender, '@bUsage@b: %s @b%s@b' % (command, cmd[4] if len(cmd) > 4 else 'argument'))
		else:
			try:
				cmd[0](self, manager, opt_dict, larg, channel, sender, userinfo)
			except Exception, e:
				tb = traceback.extract_tb(sys.exc_info()[2])
				longest = 0

				for entry in tb:
					length = len(entry[2])

					if length > longest:
						longest = length

				self.elog.exception('%s%s > @b%s@b: %s' % (sender, ':%s' % channel if channel != sender else '', full_command, e))
				self.log.exception("internets error!")

				for entry in tb:
					self.elog.traceback('@b%-*s@b : %d %s' % (longest, entry[2], entry[1], entry[3]))

				self.msg(channel, 'An exception occurred and has been reported to the developers. If this error persists please do not use the faulty command until it has been fixed.')

		parser.destroy()
		self.elog.debug('Parsed command @b%s@b: execution terminated.' % full_command)

	def onChanModes(self, prefix, channel, modes):
		if not self.initialized:
			return

		if not modes == '-z':
			return

		if channel in self.channels:
			self.channels.remove(channel)
			self.elog.request('Channel @b%s@b was dropped. Deleting it.' % channel)

	def getCommands(self):
		return self.commands_admin

	def get_location(self, opts, arg, channel, sender):
		nick = None
		location = None
		
		if 'nick' in opts:
			nick = arg
		elif arg == '':
			nick = sender
		else:
			location = arg

		if nick:
			location = self.users.get(nick, 'location')

		if not location:
			if 'nick' in opts:
				self.msg(channel, 'No location found linked to nick %s.' % arg)
			else:
				self.msg(channel, 'No location found linked to your nick. To link one, type: @b%sregister_location <location>@b' % self.commands_user.get_prefix())

		return location

	def get_steamid(self, opts, arg, channel, sender):
		"""Gets the steamid from the database."""
		nick = None
		steamid = None

		if 'nick' in opts:
			nick = arg
		else:
			nick = sender

		steamid = self.users.get(nick, 'steamid')

		if not steamid:
			if 'nick' in opts:
				self.msg(channel, 'No steamid found linked to nick %s.' % arg)
				return
			else:
				self.msg(channel, 'No steamid found linked to your nick. To link one, type: @b%sregister_steam <steamid>@b' % self.commands_user.get_prefix())
				return
		else:
			return SteamUser(nick, steamid)
