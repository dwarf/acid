import json
import re
from urllib import urlencode
from feed import HtmlFeed, XmlFeed, get_json
from utils import unescape

class Google(object):
	def __init__(self, api_key):
		self.api_key = api_key
	
	def search(self, query, userip=None):
		url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&'
		parameters = {'q': query}
		if userip:
			parameters['userip'] = userip
		url += urlencode(parameters)
		json = get_json(url)
		return json
	
	def image_search(self, query, userip=None):
		url = 'http://ajax.googleapis.com/ajax/services/search/images?v=1.0&'
		parameters = {'q': query}
		if userip:
			parameters['userip'] = userip
		url += urlencode(parameters)
		json = get_json(url)
		return json

	def yt_search(self, query, num=0, userip=None):
		url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&order=relevance&type=video&maxResults=5&'
		parameters = {'q': query, 'key': self.api_key}
		if userip:
			parameters['userip'] = userip
		
		url += urlencode(parameters)
		js = get_json(url)
		if not js['items']:
			return None
		
		video_info = js['items'][num]
		url = 'https://www.googleapis.com/youtube/v3/videos?&part=contentDetails,statistics&'
		parameters = {'id': video_info['id']['videoId'], 'key': self.api_key}
		if userip:
			parameters['userip'] = userip
		
		url += urlencode(parameters)
		js = get_json(url)
		video_details = js['items'][0]
		m = re.search(r'P((?:\d)+D)?T?(\d+H)?(\d+M)?(\d+S)?', video_details['contentDetails']['duration'])
		days, hours, minutes, seconds = map(lambda x: int(x[:-1]) if x else 0, m.groups())
		stats = video_details['statistics']
		
		return {
			'title': video_info['snippet']['title'],
			'duration': 86400 * days + 3600 * hours + 60 * minutes + seconds,
			'uploaded': video_info['snippet']['publishedAt'],
			'id': video_info['id']['videoId'],
			'favorite_count': int(stats['favoriteCount']),
			'view_count': int(stats['viewCount']),
			'liked': int(stats['likeCount']),
			'disliked': int(stats['dislikeCount'])
			}
