import utils
from datetime import datetime
from fnmatch import fnmatch
from operator import itemgetter
from socket import getaddrinfo, gaierror
import moo_utils

import pyva_net_rizon_acid_core_User as User

class Ban(object):
	def __init__(self, row):
		self.nick, self.reason, self.banned_by, self.date = row
			
class Blacklist(object):
	def __init__(self, row):
		self.vhost, self.added_by, self.reason, self.date = row

class Request(object):
	def __init__(self, row):
		self.nickname, self.main, self.vhost, self.resolved_by, self.waited_for, self.date = row
		
class RejectedRequest(Request):
	def __init__(self, row):
		Request.__init__(self, row[:-1])
		self.reason = row[-1]

class Suspicious(object):
	def __init__(self, row):
		self.vhost, self.added_by, self.reason, self.date = row

URL = 'http://s.rizon.net/vhost'
RULES = 'For basic vhost rules and restrictions, see: %s' % URL

class RequestManager(object):
	def __init__(self, module):
		self.module = module
		self.dbp = module.dbp
		self.list = {}
		self.banlist = {}
		
		self.dbp.execute("""CREATE TABLE IF NOT EXISTS `moo_bans` (
`id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`nickname` VARCHAR( 31 ) NOT NULL UNIQUE KEY ,
`reason` TEXT NULL ,
`banned_by` VARCHAR( 31 ) NOT NULL ,
`date` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)""")
		self.dbp.execute('SELECT nickname, reason, banned_by, date FROM moo_bans')
		for row in self.dbp.fetchall():
			self.banlist[row[0].lower()] = Ban(row)
		
		self.dbp.execute("""CREATE TABLE IF NOT EXISTS `moo_requests` (
`id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`nickname` VARCHAR( 31 ) NOT NULL ,
`main` VARCHAR ( 31 ) NOT NULL ,
`vhost` VARCHAR( 64 ) NOT NULL ,
`resolved_by` VARCHAR( 31 ) NULL DEFAULT NULL ,
`waited_for` MEDIUMINT UNSIGNED NOT NULL ,
`date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)""")
		self.dbp.execute('SELECT nickname, main, vhost, resolved_by, waited_for, date FROM moo_requests ORDER BY date ASC')
		self.requests = [Request(req) for req in self.dbp.fetchall()]
		
		self.dbp.execute("""CREATE TABLE IF NOT EXISTS `moo_rejections` (
`id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`nickname` VARCHAR( 31 ) NOT NULL ,
`main` VARCHAR ( 31 ) NOT NULL ,
`vhost` VARCHAR( 64 ) NOT NULL ,
`resolved_by` VARCHAR( 31 ) NULL DEFAULT NULL ,
`reason` VARCHAR ( 512 ) NOT NULL ,
`waited_for` MEDIUMINT UNSIGNED NOT NULL ,
`date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)""")
		self.dbp.execute('SELECT nickname, main, vhost, resolved_by, waited_for, date, reason FROM moo_rejections ORDER BY date ASC')
		self.rejections = [RejectedRequest(req) for req in self.dbp.fetchall()]
		
		self.dbp.execute("""CREATE TABLE IF NOT EXISTS `moo_blacklist` (
`id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`vhost` VARCHAR( 64 ) NOT NULL UNIQUE KEY ,
`added_by` VARCHAR( 31 ) NOT NULL ,
`reason` TEXT NULL ,
`date` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)""")
		self.dbp.execute('SELECT vhost, added_by, reason, date FROM moo_blacklist')
		self.blacklist = [Blacklist(row) for row in self.dbp.fetchall()]
		
		self.dbp.execute("""CREATE TABLE IF NOT EXISTS `moo_suspicious` (
`id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`vhost` VARCHAR( 64 ) NOT NULL UNIQUE KEY ,
`added_by` VARCHAR( 31 ) NOT NULL ,
`reason` TEXT NULL ,
`date` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)""")
		self.dbp.execute('SELECT vhost, added_by, reason, date FROM moo_suspicious')
		self.suspicious = [Suspicious(row) for row in self.dbp.fetchall()]
		
	def add(self, vhost, nick, main=None):
		if main:
			last_req = self.__get_last_request(main)
			acceptable, rating, internal = self.__verify(nick, vhost, main)
		else: # only for requests missed during downtime
			userinfo = User.findUser(nick)
			if userinfo and userinfo['su']:
				main = userinfo['su']
				last_req = self.__get_last_request(main)
				acceptable, rating, internal = self.__verify(nick, vhost, main)
			else: # user no longer online or SU is empty, can't check main nick / last request
				last_req = self.__get_last_request(nick)
				acceptable, rating, internal = self.__verify(nick, vhost)
		
		if not acceptable:
			self.module.msg('HostServ', 'REJECT %s %s' % (nick, rating if rating else ''))
			self.module.msg(self.module.chan, 'Rejected vhost for @b%(nick)s@b.%(int)s' % {
				'nick': nick, 
				'int' : ' %s' % internal if internal else ''})
			now = datetime.now()
			t = (nick, main, vhost, 'py-moo', 0, now, rating)
			r = RejectedRequest(t)
			if r.main:
				self.dbp.execute('INSERT INTO moo_rejections (nickname, main, vhost, resolved_by, waited_for, reason) VALUES (%s, %s, %s, %s, %s, %s)', 
								(r.nickname, r.main, r.vhost, r.resolved_by, r.waited_for, r.reason))
			else:
				r.main = r.nickname
			self.rejections.append(r)
			return
		
		if nick.lower() in self.list:
			if self.list[nick.lower()]['vhost'] == vhost:
				return # don't repeat if no change
			else:
				id = self.list[nick.lower()]['id'] # replace the old request with the new vhost
		else:
			id = self.__get_id()
		
		suspicious = self.__is_suspicious(vhost)
		date = datetime.now()
		self.list[nick.lower()] = {'id': id, 'vhost': vhost, 'rating': rating, 'date': date, 'main': main, 'nick': nick, 'last': last_req, 'suspicious': suspicious}
		self.module.msg(self.module.chan, moo_utils.format_last_req(last_req, '[new] %(id)d.@o %(nick)s @sep %(vhost)s%(susp)s @sep%(lastreq)s' % {
				'id'     : id,
				'nick'   : nick,
				'vhost'  : vhost if not suspicious else '@c10@b[!]@o ' + vhost,
				'susp'   : ' @sep @bSuspicious@b %s' % suspicious if suspicious else '',
				'lastreq': ' @bLast request@b %s ago as %s @sep' % (utils.get_timespan(last_req.date), last_req.nickname) if last_req else ''}))
	
	def add_blacklist(self, vhost, by, reason=None):
		b = (vhost, by, reason, datetime.now())
		self.dbp.execute('INSERT INTO moo_blacklist (vhost, added_by, reason) VALUES (%s, %s, %s)  ON DUPLICATE KEY UPDATE added_by=%s, reason=%s', 
						b[:-1] + (by, reason))
		for bla in self.blacklist:
			if bla.vhost.lower() == vhost.lower():
				self.blacklist.remove(bla)
				break
		
		self.blacklist.append(Blacklist(b))
		self.module.msg(self.module.chan, '@b%s@b added @b%s@b to blacklisted vHost list' % (by, vhost))
	
	def del_blacklist(self, vhost, by):
		for bla in self.blacklist:
			if bla.vhost.lower() == vhost.lower():
				self.blacklist.remove(bla)
				self.dbp.execute('DELETE FROM moo_blacklist WHERE vhost = %s', bla.vhost)
				self.module.msg(self.module.chan, '@b%s@b removed @b%s@b from blacklisted vHost list' % (by, vhost))
				return
		
		self.module.msg(self.module.chan, '@b%s@b not found in blacklisted vHost list' % vhost)
	
	def add_suspicious(self, vhost, by, reason=None):
		s = (vhost, by, reason, datetime.now())
		self.dbp.execute('INSERT INTO moo_suspicious (vhost, added_by, reason) VALUES (%s, %s, %s)  ON DUPLICATE KEY UPDATE added_by=%s, reason=%s', 
						s[:-1] + (by, reason))
		for sus in self.suspicious:
			if sus.vhost.lower() == vhost.lower():
				self.suspicious.remove(sus)
				break
		
		self.suspicious.append(Suspicious(s))
		self.module.msg(self.module.chan, '@b%s@b added @b%s@b to suspicious vHost list' % (by, vhost))
	
	def del_suspicious(self, vhost, by):
		for sus in self.suspicious:
			if sus.vhost.lower() == vhost.lower():
				self.suspicious.remove(sus)
				self.dbp.execute('DELETE FROM moo_suspicious WHERE vhost = %s', sus.vhost)
				self.module.msg(self.module.chan, '@b%s@b removed @b%s@b from suspicious vHost list' % (by, vhost))
				return
		
		self.module.msg(self.module.chan, '@b%s@b not found in suspicious vHost list' % vhost)

	def __get_nick(self, list, s):
		if s in list:
			return s
		else:
			for k, v in list.iteritems():
				if k == str(s).lower() or str(v['id']) == str(s):
					return k
			
			return None
	
	def __get_last_request(self, nick):
		nick = nick.lower()
		for r in reversed(self.requests):
			if r.main.lower() == nick or r.nickname.lower() == nick:
				return r
		
		return None
	
	def __get_id(self):
		id = 1
		if not self.list:
			return 1
		while(True):
			sorted_list = sorted(map(itemgetter('id'), self.list.values()))
			for req in sorted_list:
				if req != id:
					return id

				id = id + 1
			
			return len(sorted_list) + 1

	def approve(self, s, resolved_by, silent=False):
		nick = self.__get_nick(self.list, s)
		if not nick:
			self.module.msg(self.module.chan, 'No pending vHost request for nick/id @b%s@b' % s)
			return
		
		req = self.list[nick]
		now = datetime.now()
		t = (req['nick'], req['main'], req['vhost'], resolved_by, (now - req['date']).seconds, now)
		r = Request(t)
		if r.main:
			self.dbp.execute('INSERT INTO moo_requests (nickname, main, vhost, resolved_by, waited_for) VALUES (%s, %s, %s, %s, %s)', t[:-1])
		else: #if the request was made while moo was offline and it couldn't check main nick, it will consider the requesting nick as main nick for that group
			r.main = r.nickname
		
		self.requests.append(r)
		del self.list[nick]
		
		if silent:
			return # was already manually approved
		
		self.module.msg('HostServ', 'ACTIVATE %s' % req['nick'])
		self.module.msg(self.module.chan, 'Activated vhost for @b%s@b' % req['nick'])
#		self.module.notice(nick, 'Your requested vHost has been approved. Type "/msg HostServ ON" to activate it.')
	
	def ban(self, nick, banned_by, reason):
		nick = nick.lower()
		b = Ban((nick, reason, banned_by, datetime.now()))
		self.banlist[nick.lower()] = b
		self.dbp.execute('INSERT INTO moo_bans (nickname, reason, banned_by) VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE reason=%s, banned_by=%s',
						(nick, reason, banned_by, reason, banned_by))
		self.module.msg('HostServ', 'DELALL %s' % nick)
		self.module.msg(self.module.chan, '@b%s@b banned nick @b%s@b with reason @b%s@b. Deleted vHosts for all nicks in group @b%s@b.' % (
			banned_by, nick, reason if reason else 'No reason', nick))
	
	def list_vhosts(self, nick):
		approved = [req for req in self.requests if req.main.lower() == nick.lower() or req.nickname.lower() == nick.lower()]
		rejections = [req for req in self.rejections if req.main.lower() == nick.lower() or req.nickname.lower() == nick.lower()]
		merge = sorted(approved + rejections, key=lambda x: x.date)
		if merge:
			self.module.msg(self.module.chan, '@b[list]@b %d entries found for nick @b%s@b:' % (len(merge), nick))
			for n, r in enumerate(merge):
				is_rejection = r in rejections
				sepc = 4 if is_rejection else 3
				ago = utils.get_timespan(r.date)
				wait = int(r.waited_for / 60)
				self.module.msg(self.module.chan, '@b%(num)d.@b @bNick@b %(nick)s %(sep)s %(ago)s ago %(sep)s @bvHost@b %(vhost)s %(sep)s @bResolved by@b %(appr)s in %(wait)s minute%(s)s %(sep)s%(reason)s' % {
					'num'   : n + 1,
					'sep'   : '@b@c%s::@o' % sepc,
					'ago'   : ago,
					'nick'  : r.nickname,
					'vhost' : r.vhost,
					'appr'  : r.resolved_by,
					'wait'  : wait,
					'reason': ' @bReason@b %(r)s %(sep)s' % {
									'sep': '@b@c%s::@o' % sepc,
									'r': r.reason} if is_rejection else '',
					's'     : 's' if wait != 1 else ''})
		else:
			self.module.msg(self.module.chan, 'No previous requests by nick @b%s@b' % nick)
	
	def reject(self, s, resolved_by, reason=None, internal=None):
		nick = self.__get_nick(self.list, s)
		if not nick:
			self.module.msg(self.module.chan, 'No pending vHost request for nick/id @b%s@b' % s)
			return
		
		req = self.list[nick]
		self.module.msg('HostServ', 'REJECT %s %s' % (req['nick'], reason if reason else RULES))
		del self.list[nick]
		self.module.msg(self.module.chan, 'Rejected vhost for @b%(nick)s@b.%(int)s' % {
			'nick': req['nick'], 
			'int' : ' %s' % internal if internal else ''})
		
		now = datetime.now()
		t = (req['nick'], req['main'], req['vhost'], resolved_by, (now - req['date']).seconds, now, reason if reason else RULES)
		r = RejectedRequest(t)
		if r.main:
			self.dbp.execute('INSERT INTO moo_rejections (nickname, main, vhost, resolved_by, waited_for, reason) VALUES (%s, %s, %s, %s, %s, %s)', 
								(r.nickname, r.main, r.vhost, r.resolved_by, r.waited_for, r.reason))
		else:
			r.main = r.nickname
		self.rejections.append(r)
		
	def delete(self, nick): # remove this request from pending requests, used when manually approving/rejecting with hostserv
		del self.list[nick.lower()]		
	
	def search(self, vhost):
		list = [req for req in self.requests if req.vhost.lower() == vhost.lower()]
		if list:
			self.module.msg(self.module.chan, '@b[search]@b %d entries found for vhost @b%s@b' % (len(list), vhost))
			for n, r in enumerate(list):
				ago = utils.get_timespan(r.date)
				self.module.msg(self.module.chan, '@b%(num)d.@b %(ago)s ago @sep @bNick@b %(nick)s @sep @bvHost@b %(vhost)s @sep @bApproved by@b %(appr)s @sep' % {
					'num': n + 1,
					'ago': ago,
					'nick': r.nickname,
					'vhost': r.vhost,
					'appr': r.resolved_by})
		else:
			self.module.msg(self.module.chan, 'vHost @b%s@b was never requested' % vhost)
	
	def unban(self, nick):
		is_banned, reason, by, date = self.__is_banned(nick)
		if not is_banned:
			self.module.msg(self.module.chan, 'No bans found for nick @b%s@b' % nick)
			return
		
		self.dbp.execute('DELETE FROM moo_bans WHERE nickname = %s', (nick,))
		del self.banlist[nick.lower()]
		self.module.msg(self.module.chan, 'Unbanned nick @b%s@b' % nick)
	
	def __verify(self, nick, vhost, main=None):
		is_banned, reason, by, date = self.__is_banned(nick)
		if is_banned:
			return (False, 
				'You have been banned from requesting vHosts. For more information, join #services', 
				'vHost: %s. Ban reason: %s - %s ago' % (vhost, reason if reason else 'No reason', utils.get_timespan(date)))
		
		if main and main != nick:
			is_banned, reason, by, date = self.__is_banned(main)
			if is_banned:
				return (False, 
					'You have been banned from requesting vHosts%s.' % (('. Reason: %s' % reason) if reason else ''), 
					'vHost: %s. Ban reason: %s - %s ago' % (vhost, reason if reason else 'No reason', utils.get_timespan(date)))
		
		is_blacklisted, reason, int = self.__is_blacklisted(vhost)
		if is_blacklisted:
			return (False, reason, int)
		
		is_resolvable, resolved, host = self.__is_resolvable(vhost)
		if is_resolvable:
			return (False, 
				'Your vHost resolves. %s' % RULES, 
				'vHost resolves (%s => %s)' % (host, resolved))
		
		for req in self.list.values():
			if main == req['main'] and nick != req['nick']:
				return (False,
					'You already have a pending vHost request for a grouped nickname. Please wait for it to be reviewed before requesting a new one.',
					'Has a pending request with grouped nick %s' % req['nick'])
		
		return (True, '@c9Acceptable@c', None)
	
	def __is_resolvable(self, host):
		try:
			res = getaddrinfo(host, 80)
			if res[0][4][0] == '127.0.53.53': # https://www.icann.org/news/announcement-2-2014-08-01-en
				return (False, None, None)
			return (True, res[0][4][0], host)
		except gaierror, e:
			return (False, None, None)
		except UnicodeError, e:
			return (False, None, None)
	
	def __is_banned(self, nick):
		nick = nick.lower()
		if nick in self.banlist:
			b = self.banlist[nick]
			return (True, b.reason, b.banned_by, b.date)
		
		return (False, None, None, None)
		
	def __is_blacklisted(self, host):
		h = host.lower()
		for b in self.blacklist:
			if (fnmatch(h, b.vhost.lower())):
				return (True, RULES, 'Host matches %s (%s)' % (b.vhost, host))
		
		return (False, None, None)
	
	def __is_suspicious(self, host):
		h = host.lower()
		for b in self.suspicious:
			if (fnmatch(h, b.vhost.lower())):
				return 'matches @c10%s@c%s' % (b.vhost, ' (%s)' % b.reason if b.reason else '')
		
		return False
