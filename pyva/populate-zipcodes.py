#!/usr/bin/env python2
# PyPsd's Internets bot's zip code importer, for the weather commands
# This is run separately to PyPsd itself, before if you want weather ZIP codes to work

import os
import sys
import codecs
import ConfigParser
import MySQLdb as db

# find config file and zipcode csv file
config_filename = 'config.ini'
if not os.path.exists(config_filename):
    config_filename = os.path.join('..', config_filename)
    if not os.path.exists(config_filename):
        print "We cannot locate the pypsd/pyva config.ini file"
        print 'This script should be run from the root acid/ directory'
        exit(1)

zipcode_csv_file = 'weather-zipcodes.csv'
if not os.path.exists(zipcode_csv_file):
    zipcode_csv_file = os.path.join('data', 'weather-zipcodes.csv')
    if not os.path.exists(zipcode_csv_file):
        zipcode_csv_file = os.path.join('pyva', zipcode_csv_file)
        if not os.path.exists(zipcode_csv_file):
            print "We cannot locate the zipcode csv file"
            print 'weather-zipcodes.csv should be in the working dir, in data/ , or in pyva/data/'
            exit(1)

# options
drop_table = '--drop' in sys.argv
debug = '--debug' in sys.argv

# config
config = ConfigParser.ConfigParser()
config.readfp(codecs.open(config_filename, 'r', 'utf8'))

# make db pointer
dbx = db.connect(
    host=config.get('database', 'host'),
    user=config.get('database', 'user'),
    passwd=config.get('database', 'passwd'),
    db=config.get('database', 'db'),
)
dbx.autocommit(True)  # no need to have transactions
dbp = dbx.cursor()

# check if table exists
dbp.execute("SELECT count(*) FROM information_schema.tables t WHERE t.table_name='zipcode_citystate';")
if dbp.fetchone()[0] == 0:
    table_exists = False
else:
    table_exists = True
rows = 0  # zero rows since table doesn't exist

# clearing/setting table information
try:
    # check if we should drop the table, to basically 'refresh'and start again
    if drop_table:
        if table_exists:
            dbp.execute("DROP TABLE zipcode_citystate;")
            print 'Current ZIP table dropped.'
            table_exists = False
            exit()
        else:
            print 'ZIP table does not exist, cannot drop.'
            exit()

    if table_exists:
        # count current rows, see if we have any info in there already
        dbp.execute("SELECT count(*) FROM zipcode_citystate;")
        rows = int(dbp.fetchone()[0])
        if rows > 0:
            print 'ZIP codes already exist in database. Please run:'
            print sys.argv[0], '--drop'
            print 'in order to clear the current ZIP table and populate with new ZIP codes'
            exit(1)
    else:
        print 'ZIP Table does not yet exist.'
except Exception, err:
    print 'Error setting up initial table information: {}'.format(err)
    raise

# create table
try:
    dbp.execute("CREATE TABLE IF NOT EXISTS zipcode_citystate (zipcode INT(5), city VARCHAR(15) NOT NULL, state VARCHAR(2) NOT NULL, UNIQUE KEY (zipcode));")
    print 'ZIP Table created.'
except Exception, err:
    print 'Error creating database table zipcode_citystate: {}'.format(err)
    raise

# load zipfile contents
if rows == 0:
    with open(zipcode_csv_file, 'rb') as zipfile:
        print 'Populating ZIP Table'

        current_row = 0
        first_line = True  # contains row info, not necessary and breaks our int/float/etc

        for line in zipfile:
            if first_line:
                first_line = False
                continue

            # insertion
            line = line.replace('\n', '').replace('"', '').split(',')
            if len(line) > 3:
                zipcode, city, state, latitude, longitude, timezone, dst = line

                try:
                    dbp.execute("INSERT INTO zipcode_citystate (zipcode, city, state) VALUES (%s,%s,%s);", [int(zipcode), city, state])
                    if debug:
                        print 'Inserted ZIP code', zipcode, ':', city, ':', state
                except Exception, err:
                    print 'Error adding ZIP code to database: ({})'.format(err)
                    print line
                    print zipcode, city, state
                    raise

            # status
            current_row += 1
            if current_row % 1000 == 0:  # print every 1k rows
                print current_row, 'rows inserted'

        print current_row, 'rows inserted in total'
