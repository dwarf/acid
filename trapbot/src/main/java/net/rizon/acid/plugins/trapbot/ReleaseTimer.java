package net.rizon.acid.plugins.trapbot;

import java.util.Date;
import java.util.Random;

import net.rizon.acid.core.Message;
import net.rizon.acid.core.Protocol;
import net.rizon.acid.core.Timer;

class ReleaseTimer extends Timer
{
	ReleaseTimer()
	{
		// XXX magic?
		super(new Random().nextInt(2700) + 2700, false);
	}

	@Override
	public void run(final Date now)
	{
		Protocol.privmsg(trapbot.trapbot.getUID(), trapbot.getTrapChanName(), Message.BOLD + "YOU HAVE 1 MINUTE TO PART THE CHANNEL, GET OUT WHILE YOU CAN!" + Message.BOLD);

		trapbot.enforce = false;
		trapbot.releaseTimer = null;

		trapbot.retrapTimer = new RetrapTimer(60, false);
		trapbot.retrapTimer.start();
	}
}
