package net.rizon.acid.core;

import java.util.LinkedList;

public abstract class Event
{
	public Event()
	{
		events.add(this);
	}

	public void remove()
	{
		events.remove(this);
	}

	public void onUserConnect(final User u) { }

	public void onNickChange(final User u, final String oldnick) { }

	public void onJoin(Channel channel, User[] users) { }

	public void onPart(User user, Channel channel) { }

	public void onKick(String kicker, User victim, Channel channel, String reason) { }

	public boolean onPrivmsg(final String creator, final String recipient, final String msg) { return false; }
	public void onCommandCertFPMismatch(final User u, final String certfp) { }

	public boolean onNotice(final String creator, final String recipient, final String msg) { return false; }

	public boolean onCTCP(final String creator, final String recipient, final String msg) { return false; }
	public void onCTCPReply(User source, String target, String message) { }

	public void onSync() { }
	public void onEOB(Server server) { }

	public void onKill(final String killer, User user, final String reason) { }

	public void onQuit(User user, String msg) { }

	public void onServerLink(Server server) { }

	public void onServerDelink(Server server) { }

	public void onUserMode(User user, String oldmodes, String newmodes) { }
	public void onChanModes(String prefix, Channel chan, String modes) { }

	public void onRehash() throws Exception { }

	private static LinkedList<Event> events = new LinkedList<Event>();

	public static final Event[] getEvents()
	{
		Event[] a = new Event[events.size()];
		events.toArray(a);
		return a;
	}
}
