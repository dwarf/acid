package net.rizon.acid.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;

public abstract class Timer
{
	private static final Logger log = Logger.getLogger(Timer.class.getName());
	private static ArrayList<Timer> timers = new ArrayList<Timer>();

	public Date creation;
	public Date tick;
	public long time_from_now;
	public boolean repeating;
	private boolean dead;

	public Timer(long time_from_now, boolean repeating)
	{
		this.creation = new Date();
		this.tick = new Date(System.currentTimeMillis() + (time_from_now * 1000));
		this.time_from_now = time_from_now;
		this.repeating = repeating;
		this.dead = false;
	}

	public void start()
	{
		timers.add(this);
	}

	public void stop()
	{
		this.dead = true;
	}

	public abstract void run(final Date now);

	public static void processTimers()
	{
		Date now = new Date();

		// Timers can be added from Timer;:run, which will modify this list.
		// Iterate by indexes instead going backward.
		for (int i = timers.size(); i >  0; --i)
		{
			Timer t = timers.get(i - 1);

			if (t.dead == false && now.after(t.tick))
			{
				try
				{
					t.run(now);
				}
				catch (Exception ex)
				{
					log.log(Level.SEVERE, "Error running timer", ex);
				}
				if (t.repeating == false)
					t.stop();
				else
					t.tick = new Date(System.currentTimeMillis() + (t.time_from_now * 1000));
			}
		}

		for (Iterator<Timer> it = timers.iterator(); it.hasNext();)
		{
			Timer t = it.next();

			if (t.dead == true)
				it.remove();
		}
	}
}