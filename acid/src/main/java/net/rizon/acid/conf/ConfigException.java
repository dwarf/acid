package net.rizon.acid.conf;

@SuppressWarnings("serial")
public class ConfigException extends Exception
{
	ConfigException(String what)
	{
		super(what);
	}
}