package net.rizon.acid.conf;

public class ServerInfo implements Validatable
{
	public String name, description, id;

	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotEmpty("name", name);
		Validator.validateNotEmpty("description", description);
		Validator.validateNotEmpty("id", id);
	}
}