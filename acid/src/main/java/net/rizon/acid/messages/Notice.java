package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;

public class Notice extends Message
{
	public Notice()
	{
		super("NOTICE");
	}

	// :99hAAAAAB NOTICE moo :BOO !

	@Override
	public void onUser(User sender, String[] params)
	{
		String msg = params[1];
		if (msg.startsWith("\1") && msg.endsWith("\1"))
			Acidictive.onCtcpReply(sender, params[0], params[1]);
		else
			Acidictive.onNotice(sender.getNick(), params[0], params[1]);
	}
}
