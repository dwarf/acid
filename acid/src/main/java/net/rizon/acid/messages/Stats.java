package net.rizon.acid.messages;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Protocol;

public class Stats extends Message
{
	public Stats()
	{
		super("STATS");
	}

	//:0XYAAAAAI STATS u :acid.rizon.net

	@Override
	public void on(String source, String[] params)
	{
		String letter = params[0];
		net.rizon.acid.core.Server targ = net.rizon.acid.core.Server.findServer(params[1]);

		if (targ != Acidictive.me)
		{
			Protocol.numeric(219, source + " " + letter + " :End of /STATS report");
			return;
		}

		if (letter.equals("u"))
		{
			long now = (System.currentTimeMillis() - Acidictive.startTime)/1000;
			Protocol.numeric(242, String.format("%s :Server Up %d days, %02d:%02d:%02d",
					source, now/86400, (now/3600) % 24, (now/60) % 60, now % 60));
		}
		else if (letter.equalsIgnoreCase("o"))
		{
			try
			{
				PreparedStatement stmt = Acidictive.acidcore_sql.prepare("SELECT `user`, `flags` FROM `access`");
				ResultSet rs = Acidictive.acidcore_sql.executeQuery(stmt);
				while (rs.next())
					Protocol.numeric(243, source + " " + letter + " *@* * " + rs.getString("user") + " " + rs.getString("flags") + " opers");
			}
			catch (SQLException ex)
			{
			}
		}
		else if (letter.equals("U"))
		{
			for (final String s : Acidictive.conf.general.ulines)
				Protocol.numeric(248, source + " U " + s + " *@* cKUXYQRLDE");
		}

		Protocol.numeric(219, source + " " + letter + " :End of /STATS report");
	}
}
